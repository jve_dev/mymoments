//
//  SelectedPostViewController.h
//  BSTut1
//
//  Created by Javier Evelyn on 1/12/14.
//  Copyright (c) 2014 Javier Evelyn. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Post.h"
#import "AppDelegate.h"

@interface SelectedPostViewController : UIViewController<UITextViewDelegate>

@property (strong, nonatomic) IBOutlet UIImageView *selectedPostImageView;
@property (strong, nonatomic) IBOutlet UITextView *selectedPostTextView;

@property (strong, nonatomic) NSManagedObjectContext *moc;
@property (strong, nonatomic) AppDelegate *appDelegate;
@property (strong, nonatomic) Post *selectedPost;

- (IBAction)backButton:(id)sender;
- (IBAction)shareButton:(id)sender;
@end
