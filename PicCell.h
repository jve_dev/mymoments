//
//  PicCell.h
//  BSTut1
//
//  Created by Javier Evelyn on 1/11/14.
//  Copyright (c) 2014 Javier Evelyn. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PicCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *dateLabel;
@property (strong, nonatomic) IBOutlet UILabel *commentsLabel;
@property (strong, nonatomic) IBOutlet UIImageView *postImageView;

@end
