//
//  SelectedPostViewController.m
//  BSTut1
//
//  Created by Javier Evelyn on 1/12/14.
//  Copyright (c) 2014 Javier Evelyn. All rights reserved.
//

#import "SelectedPostViewController.h"

@interface SelectedPostViewController ()

@end

@implementation SelectedPostViewController

#pragma mark - View Life Cycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    _appDelegate = (AppDelegate*)[[UIApplication sharedApplication]delegate];
    _moc = [_appDelegate managedObjectContext];
    
    _selectedPostImageView.image = [UIImage imageWithData:_selectedPost.pic];
    _selectedPostTextView.text = _selectedPost.comment;
}

#pragma mark - IBActions

- (IBAction)backButton:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)shareButton:(id)sender
{
    NSString *textToShare = _selectedPostTextView.text;
    UIImage *imageToShare = _selectedPostImageView.image;
    NSArray *itemsToShare = @[textToShare, imageToShare];
    UIActivityViewController *activityVC = [[UIActivityViewController alloc] initWithActivityItems:itemsToShare applicationActivities:nil];
    activityVC.excludedActivityTypes = @[UIActivityTypePrint, UIActivityTypeCopyToPasteboard, UIActivityTypeSaveToCameraRoll];     [self presentViewController:activityVC animated:YES completion:nil];
}
@end
