//
//  AddPostViewController.h
//  BSTut1
//
//  Created by Javier Evelyn on 1/11/14.
//  Copyright (c) 2014 Javier Evelyn. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PicCell.h"
#import "MainViewController.h"
#import "Post.h"

@interface AddPostViewController : UIViewController<UIImagePickerControllerDelegate, UINavigationControllerDelegate>

@property (strong, nonatomic) IBOutlet UITextView *commentTextView;
@property (strong, nonatomic) IBOutlet UILabel *dateLabel;
@property (strong, nonatomic) IBOutlet UIImageView *postImageViewFirstVC;

@property (strong, nonatomic) Post *myPosts;
@property (strong, nonatomic) NSManagedObjectContext *moc;

- (IBAction)saveButton:(id)sender;
- (IBAction)closeButton:(id)sender;
- (IBAction)takePhotoButton:(id)sender;

@end
