//
//  MainViewController.h
//  BSTut1
//
//  Created by Javier Evelyn on 1/11/14.
//  Copyright (c) 2014 Javier Evelyn. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Post.h"

@interface MainViewController : UIViewController<UITableViewDelegate, UITableViewDataSource>

@property (strong, nonatomic) Post *postNew;
@property (strong, nonatomic) NSManagedObjectContext *moc;
@property (strong, nonatomic) NSArray *postArray;

@property (strong, nonatomic) IBOutlet UITableView *postTableView;

- (IBAction)addPost:(id)sender;
@end
