//
//  AddPostViewController.m
//  BSTut1
//
//  Created by Javier Evelyn on 1/11/14.
//  Copyright (c) 2014 Javier Evelyn. All rights reserved.
//

#import "AddPostViewController.h"
#import "AppDelegate.h"

@interface AddPostViewController ()
{
    AppDelegate *appDelegate;
}

@end

@implementation AddPostViewController

#pragma mark - View Life Cycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    appDelegate = (AppDelegate*)[[UIApplication sharedApplication]delegate];
    _moc = [appDelegate managedObjectContext];
    
    NSDateFormatter *df = [[NSDateFormatter alloc]init];
    [df setDateFormat:@"MM-dd-yyyy"];
    NSDate *currentDate = [NSDate date];
    NSString *theDate = [df stringFromDate:currentDate];
    
    _dateLabel.text = theDate;
    
    _commentTextView.layer.borderColor = [[UIColor blackColor]CGColor];
    _commentTextView.layer.borderWidth = 1;
}

#pragma mark - IBActions

- (IBAction)takePhotoButton:(id)sender
{
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        UIImagePickerController *imagePickerController = [[UIImagePickerController alloc]init];
        imagePickerController.delegate = self;
        imagePickerController.allowsEditing = YES;
        imagePickerController.sourceType = UIImagePickerControllerSourceTypeCamera;
        
        [self presentViewController:imagePickerController animated:YES completion:nil];
    } else {
        UIAlertView *av = [[UIAlertView alloc]initWithTitle:@"No Camera On Device" message:@"You currently do not have the capability to take photos with this device." delegate:self cancelButtonTitle:@"Dismiss" otherButtonTitles:nil, nil];
        [av show];
        _postImageViewFirstVC.image = [UIImage imageNamed:@"zumba.png"];
    }
}

- (IBAction)saveButton:(id)sender
{
    if (_commentTextView.text == nil &&  _postImageViewFirstVC.image == nil) {
        UIAlertView *av = [[UIAlertView alloc]initWithTitle:@"Missing Info" message:@"Don't forget to add a pic and comment" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [av show];
    } else {
        //save object to core data object (Post) & dismiss FirstVC
        MainViewController *vc = [[MainViewController alloc]initWithNibName:@"MainVC" bundle:nil];
        
        _myPosts = [NSEntityDescription insertNewObjectForEntityForName:@"Post" inManagedObjectContext:_moc];
        
        [_myPosts setComment:_commentTextView.text];
        
        NSData *picData = [NSData dataWithData:UIImagePNGRepresentation(_postImageViewFirstVC.image)];
        [_myPosts setPic:picData];
        
        NSDateFormatter *df = [[NSDateFormatter alloc]init];
        [df setDateFormat:@"MM-dd-yyyy"];
        [df setDateStyle:NSDateFormatterMediumStyle];
        [_myPosts setDate:[df dateFromString:_dateLabel.text]];
        
        vc.postNew = _myPosts;
        
        NSError *error;
        [_moc save:&error];
        
        [self dismissViewControllerAnimated:YES completion:nil];
    }
}

- (IBAction)closeButton:(id)sender
{
    //dismiss FirstVC
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [[self view]endEditing:YES];
}

#pragma mark - UIImagePicker Delegate

-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    UIImage *image = [info objectForKey:UIImagePickerControllerEditedImage];
    _postImageViewFirstVC.image = image;
    [picker dismissViewControllerAnimated:YES completion:nil];
}
@end
