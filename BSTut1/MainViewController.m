//
//  MainViewController.m
//  BSTut1
//
//  Created by Javier Evelyn on 1/11/14.
//  Copyright (c) 2014 Javier Evelyn. All rights reserved.
//

#import "MainViewController.h"
#import "PicCell.h"
#import "AppDelegate.h"
#import "AddPostViewController.h"
#import "SelectedPostViewController.h"

@interface MainViewController ()
{
    AppDelegate *appDelegate;
}

@end

@implementation MainViewController

#pragma mark - Life View Cycle

- (void)viewDidLoad
{
    [super viewDidLoad];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    appDelegate = (AppDelegate*)[[UIApplication sharedApplication]delegate];
    _moc = [appDelegate managedObjectContext];
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Post" inManagedObjectContext:_moc];
    [fetchRequest setEntity:entity];
    
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"date"
                                                                   ascending:NO];
    NSArray *sortDescriptors = [[NSArray alloc] initWithObjects:sortDescriptor, nil];
    [fetchRequest setSortDescriptors:sortDescriptors];
    
    NSError *error = nil;
    NSArray *fetchedObjects = [_moc executeFetchRequest:fetchRequest error:&error];
    
    if (fetchedObjects == nil) {
        NSLog(@"%@", error);
    }
    
    [self setPostArray:fetchedObjects];
    
    [_moc save:&error];
    
    [_postTableView reloadData];
}

#pragma mark - TableView Datasource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [_postArray count];
}

- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"Cell";
    
    PicCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    
    _postNew = [_postArray objectAtIndex:indexPath.row];
    
    NSDateFormatter *df = [[NSDateFormatter alloc]init];
    [df setDateFormat:@"MM-dd-yyyy"];
    [df setDateStyle:NSDateFormatterMediumStyle];
    
    cell.dateLabel.text = [df stringFromDate: _postNew.date];
    cell.commentsLabel.text = [_postNew comment];
    cell.postImageView.image = [UIImage imageWithData:[_postNew pic]];
    
    return cell;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    NSIndexPath *indexPath = [_postTableView indexPathForCell:sender];
    Post *newSelectedPost = [_postArray objectAtIndex:indexPath.row];
    
    SelectedPostViewController *vc = [segue destinationViewController];
    vc.selectedPost = newSelectedPost;
}

#pragma mark - IBActions

- (IBAction)addPost:(id)sender
{
    AddPostViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"AddPostVC"];
    [self presentViewController:vc
                       animated:YES completion:nil];
}
@end
