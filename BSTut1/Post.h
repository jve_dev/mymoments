//
//  Post.h
//  BSTut1
//
//  Created by Javier Evelyn on 1/11/14.
//  Copyright (c) 2014 Javier Evelyn. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Post : NSManagedObject

@property (nonatomic, retain) NSData * pic;
@property (nonatomic, retain) NSString * comment;
@property (nonatomic, retain) NSDate * date;

@end
